#!/bin/bash

path = $1

# counts tweets
zcat $path | wc -l

# counts unique tweets
zcat $path | uniq | wc -l

# counts ReTweets out of unique tweets
zcat $path | uniq | grep -c '^RT'

# 20 unique tweets (no retweets)
zcat $path | uniq | grep -v '^RT' | head -20

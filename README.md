# README #
s2763443

This repository is for the project of IWO.
---------------------------------------------
The program main.py read lines from sys.stdin.
Each line should contain a tweet identifier, username and tweet text seperated by tabs.

To call the program (for all tweets from the 1st of march 2017):
zcat /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab id user text | grep -v 'RT' | python3 main.py

Example output:

Analysing tweets...
Done!

number of male tweets: 27917
number of female tweets: 19054

Total number of emoji in male tweets: 1342
Total number of emoji in female tweets: 1601

Mean of emoji per male tweet: 0.04807106780814557
Mean of emoji per female tweet: 0.08402435184213289

Welch's T-test:
H0: Mean male = mean female
HA: Mean male < mean female
P-value:
2.87886353689e-51

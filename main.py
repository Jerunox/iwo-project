#!/usr/bin/env python3

# To call this program:
# zcat /net/corpora/twitter2/Tweets/2017/03/20170301\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab id user text | grep -v 'RT' | python3 main.py

# main.py
# s2763443

import sys
from scipy import stats


def detect_gender(username,male_names,female_names):
	''' This function returns the gender of an users if detectable.'''
	gender_male = False
	gender_female = False

	for name in male_names:
		if name in username:
			gender_male = True

	for name in female_names:
		if name in username:
			gender_female = True

	if gender_male == True and gender_female == False:
		gender = "male"		
	elif gender_male == False and gender_female == True:
		gender = "female"
	else:
		gender = "None"
	return gender


def count_emoji(text,emoji_list):
	''' This function returns the number of emoji in a tweet.'''
	emoji_count = 0
	for emoji in emoji_list:
		if emoji in text:
			emoji_count += 1
	return emoji_count


def welch_t_test(male_scores,female_scores):
	''' This function calculates the P-value between both group means by using a Welch's T-Test. '''
	P_value = stats.ttest_ind(male_scores, female_scores, equal_var = False)[1]

	print("\nWelch's T-test:",file=sys.stderr)
	print("H0: Mean male = mean female",file=sys.stderr)
	print("HA: Mean male < mean female",file=sys.stderr)
	print("P-value:",file=sys.stderr)
	print(P_value)


def main():
	print("Analysing tweets...",file=sys.stderr)
	
	with open("male_names.txt") as f:
		male_names = [word[:-1] for word in f]
	with open("female_names.txt") as f:
		female_names = [word[:-1] for word in f]
	with open("emoji.txt") as f:
		emoji_list = [emoji for emoji in f]

	male_scores = []
	female_scores = []

	male_emoji = 0
	female_emoji = 0

	# Tweet_id, username and tweet text are stored in variables
	for line in sys.stdin:
		[ident,user,text] = line.split('\t')
		user = user.lower()

		# Gender of the tweet's autho is detected
		gender = detect_gender(user,male_names,female_names)

		#If the gender of a tweet is either male or female:
		#the number of emoji are counted.
		if gender == "male":
			emoji_count = count_emoji(text,emoji_list)
			male_scores.append(emoji_count)
			male_emoji = male_emoji + emoji_count

		if gender == "female":
			emoji_count = count_emoji(text,emoji_list)
			female_scores.append(emoji_count)
			female_emoji = female_emoji + emoji_count


	print("Done!\n",file=sys.stderr)
	print("number of male tweets:",len(male_scores),file=sys.stderr)
	print("number of female tweets:",len(female_scores),file=sys.stderr)

	print("\nTotal number of emoji in male tweets:",male_emoji,file=sys.stderr)
	print("Total number of emoji in female tweets:",female_emoji,file=sys.stderr)

	print("\nMean of emoji per male tweet:",male_emoji/len(male_scores),file=sys.stderr)
	print("Mean of emoji per female tweet:",female_emoji/len(female_scores),file=sys.stderr)

	welch_t_test(male_scores,female_scores)


if __name__ == "__main__":
	main()